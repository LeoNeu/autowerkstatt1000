import java.time.LocalDate;
import java.time.Period;
import java.util.LinkedList;
import java.util.Queue;

public class Werkstatt {


    private Queue<Auto> carListe;
    private int kapazitaet;
    Auftrag auftrag = new Auftrag();
    private int tage;


    public Werkstatt(int kapazitaet) {


        this.carListe = new LinkedList<Auto>();     //Kurze Notiz für Aufgabenverständnis: wann macht es Sinn oder warum überhaupt muss ich hier ein Objekt initialisieren ??
        this.kapazitaet = kapazitaet;

    }

    // Kurze Notiz für Aufgabenverständnis: immer wenn wir ein Objekt einer anderen Klasse nutzen wollen, müssen wir das Objekt mit in die Methode geben
    // Kurze Notiz für Aufgabenverständnis: damit man ein Typ des Objektes mitgeben kann

    public void repariereAuto() {
        Auto fertigesAuto = carListe.poll();
        System.out.println("Das Auto " + fertigesAuto + " ist heile. Sie können es jetzt abholen.");

    }


    public boolean volleWerkstatt() {
        if (this.carListe.size() == kapazitaet) {
            return true;
        } else {
            return false;
        }
    }

    public boolean freiePlatze() {
        if (this.carListe.size() == 0) {
            return true;
        }
        return false;
    }

    public void enter(Auto auto) {
        if (volleWerkstatt() == true) {
            System.out.println("Leider voll. Zur Zeit sind folgende Autos in der Warteschlange: " + carListe);
        } else {
            this.carListe.add(auto);
        }

    }

    // ausgabe Methode

    public void ausgabeListe() {

        System.out.println("Die Warteschlange umfasst: " + this.carListe );
    }


    // getter, setter carListe und Kapazität

    public Queue<Auto> getCarListe() {
        return carListe;
    }

    public void setCarListe(Queue<Auto> carListe) {
        this.carListe = carListe;
    }

    public int getKapazitaet() {
        return kapazitaet;
    }

    public void setKapazitaet(int kapazitaet) {
        this.kapazitaet = kapazitaet;
    }

    public void zeitRaumBerechnen () {
        LocalDate date1 = LocalDate.of(2018, 12, 12);
        LocalDate date2 = LocalDate.of(2018, 12, 24);
        Period period = Period.between(date1, date2);
        int datum = period.getDays();
        System.out.println(datum);

    }


    public int getTage() {
        return tage;
    }

    public void setTage(int tage) {
        this.tage = tage;
    }



    public int berechnungDauerReperatur (){

        for (int x=0; x<carListe.size(); x++) {


            if (carListe.size() == 1) {
                this.tage = 1;
            } else if (carListe.size() > 1) {
                this.tage = 1+x;
            }
        }
        return this.tage;
    }


    // Kurze Notiz für Aufgabenverständnis: mit int tage zählen

    // Kurze Notiz für Aufgabenverständnis: ich will nicht das Auto reingeben, sondern, die Methode selber soll entscheiden welches Auto genommen
    // Kurze Notiz für Aufgabenverständnis: zwei Möglichkeiten: wann ist das Auto fertig? entweder wenn der Mitarbeiter fertig ist oder wenn das Auto aus der Werkstatt removed wird


}
