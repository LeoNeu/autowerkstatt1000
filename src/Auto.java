public class Auto {

    private String name;


    public Auto(String name) {
        this.name = name;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    @Override
    public String toString() {
        return this.name;
    }

    /*
    toString Methode noch zur Ansicht in beiden Varianten drin
     */

//    @Override
//    public String toString() {
//        return "Auto{" +
//                "name='" + name + '\'' +
//                ", heile=" + heile +
//                '}';
//    }
}
