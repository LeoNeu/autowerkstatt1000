

public class Main {

    public static void main(String[] args) {


        // es ist morgens 8 Uhr. Die Werkstatt macht auf und 5 Autos stehen in der Warteschlange
        Werkstatt werkstatt = new Werkstatt(5);
        Auto auto1 = new Auto("bmw");
        Auto auto2 = new Auto("audi");
        Auto auto3 = new Auto("opel");
        Auto auto4 = new Auto("skoda");
        Auto auto5 = new Auto("tesla");
        Auto auto6 = new Auto("vw");

        // Die Werkstatt fügt die Autos nach der Reihenfolge der Ankunft einer List hinzu
        werkstatt.enter(auto1);
        werkstatt.enter(auto2);
        werkstatt.enter(auto3);
        werkstatt.enter(auto4);
        werkstatt.enter(auto5);

        // Die List umfasst nun folgende Autos
        werkstatt.ausgabeListe();

        //Ein weiteres Auto kommt bei der Werkstatt an - die Kapazität ist jedoch ausgereizt
        werkstatt.enter(auto6);

        //Nun wurde das erste Auto der Warteschlange repariert
        werkstatt.repariereAuto();
        werkstatt.repariereAuto();


        werkstatt.ausgabeListe();

        System.out.println("Die Wartezeit dauert zurzeit: " + werkstatt.berechnungDauerReperatur() + " Tage.");

        werkstatt.repariereAuto();


        System.out.println("Die Wartezeit dauert zurzeit: " + werkstatt.berechnungDauerReperatur() + " Tage.");




        werkstatt.enter(auto6);
        werkstatt.ausgabeListe();
        System.out.println("Die Wartezeit dauert zurzeit: " + werkstatt.berechnungDauerReperatur() + " Tage.");


        // werkstatt.zeitRaumBerechnen();











    }

}



